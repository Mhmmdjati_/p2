package com.muhammadjati10191058.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnmasuk=findViewById(R.id.Btnmasuk);
        btnmasuk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent masuk = new Intent(MainActivity.this,ContentActivity.class);
                startActivity (masuk);

            }
        });
    }
}